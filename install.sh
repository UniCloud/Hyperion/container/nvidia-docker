# prime. github.com/NVIDIA/nvidia-container-runtime ,
#nvidia.github.io/nvidia-container-runtime/ ,
#github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0)

sudo apt install curl -y

repo_docker(){
  curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | \
    sudo apt-key add -
  distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
  curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
    sudo tee /etc/apt/sources.list.d/nvidia-docker.list
  sudo apt-get update
}
repo_runtime(){
  curl -s -L https://nvidia.github.io/nvidia-container-runtime/gpgkey | \
    sudo apt-key add -
  distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
  curl -s -L https://nvidia.github.io/nvidia-container-runtime/$distribution/nvidia-container-runtime.list | \
    sudo tee /etc/apt/sources.list.d/nvidia-container-runtime.list
  sudo apt-get update
}

sudo ls
repo_docker
repo_runtime

sudo apt install nvidia-container-runtime -y

sudo apt install nvidia-docker2 -y
